FROM   python:3.8-slim

RUN apt-get update && apt-get -y install xz-utils curl && apt-get  clean
RUN curl https://nodejs.org/dist/v16.3.0/node-v16.3.0-linux-x64.tar.gz | tar -xzC /opt/
ENV PATH="/opt/node-v16.3.0-linux-x64/bin:${PATH}"
ENV NODE_PATH=/opt/node-v16.3.0-linux-x64/lib/node_modules

ENTRYPOINT [ "/bin/sh" ]